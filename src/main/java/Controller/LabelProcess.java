/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import javax.swing.JLabel;

/**
 *
 * @author _TERIO_
 */
public class LabelProcess extends Thread{
    private final JLabel cargando;
    private boolean MustDo;

    public LabelProcess(JLabel cargando) {
        this.cargando = cargando;
        this.MustDo = true;
    }

    public void endTask() {
        this.MustDo = false;
    }

    @Override
    public void run() {
        int dotcount = 0;
        while (MustDo) {
            String carg = "Cargando";
            for (int a = 0; a < dotcount; a++) {
                carg += ".";
            }
            cargando.setText(carg);
            if (dotcount < 3) {
                dotcount++;
            } else{
                dotcount = 0;
            }
            try {
                Thread.sleep(630);
            } catch (InterruptedException ex) {
            }
        }
        cargando.setText("");
    }
    
    
}
