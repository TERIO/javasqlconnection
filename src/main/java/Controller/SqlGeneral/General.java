/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller.SqlGeneral;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author _TERIO_
 */
public class General {
    public static Connection getConnection(String user, String pass, String dbName, String server) throws SQLException{
        if (server == null) {
            server = "localhost:1433";
        }
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(General.class.getName()).log(Level.SEVERE, null, ex);
        }
        Connection con = DriverManager.getConnection("jdbc:sqlserver://"+server+";databaseName="+dbName+";user="+user+";password="+pass+";");
        return con;
    }
    
    public static String[] getTableNames(Connection con, boolean view) throws SQLException{
        ResultSet rs;
        if (view) {
            rs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY).executeQuery("select TABLE_NAME from INFORMATION_SCHEMA.TABLES where TABLE_TYPE = 'VIEW'");
        } else {
            rs = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY).executeQuery("select TABLE_NAME from INFORMATION_SCHEMA.TABLES where TABLE_TYPE = 'BASE TABLE'");
        }
        String[] tableNames = new String[getRowCount(rs)];
        while (rs.next()) {            
            tableNames[rs.getRow()-1] = rs.getString(1);
        }
        return tableNames;
    }
    
    public static String[] getColumnNames(ResultSetMetaData rsmd) throws SQLException{
        String[] names = new String[rsmd.getColumnCount()];
        for (int a = 0; a < names.length; a++) {
            names[a] = rsmd.getColumnName(a+1);
        }
        return names;
    }
    
    public static DefaultTableModel getTableModel(ResultSet rs) throws SQLException{
        ResultSetMetaData rsmd = rs.getMetaData();
        String[] header = getColumnNames(rsmd);
        int rowCount = getRowCount(rs);
        if (rowCount == 0) {
            return new DefaultTableModel(header, rowCount){
                @Override
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };
        }
        String[][] model = new String[rowCount][header.length];
        while (rs.next()) {            
            for (int c = 0; c < header.length; c++) {
                model[rs.getRow()-1][c] = rs.getString(c+1);
            }
        }
        return new DefaultTableModel(model, header){
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
    }
    
    private static int getRowCount(ResultSet rs) {
        if (rs == null) {
            return 0;
        }

        try {
            rs.last();
            return rs.getRow();
        } catch (SQLException exp) {
            exp.printStackTrace();
        } finally {
            try {
                rs.beforeFirst();
            } catch (SQLException exp) {
                exp.printStackTrace();
            }
        }

        return 0;
    }
}
