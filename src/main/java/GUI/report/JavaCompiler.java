/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.report;

import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;

/**
 *
 * @author _TERIO_
 */
public class JavaCompiler {
    public static void main(String[] args) {
        try {
            JasperCompileManager.compileReportToFile("src\\main\\java\\GUI\\report\\newReport.jrxml", "src\\main\\java\\GUI\\report\\newReport.jasper");
        } catch (JRException ex) {
            Logger.getLogger(JavaCompiler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
